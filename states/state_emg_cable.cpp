#include "control.h"

/**
* Test EMG cable
*/
void Control::state_emg_cable() {

    static DigitalIn inputs[3] = {DigitalIn(PTB18, PullUp), DigitalIn(PTB19, PullUp),
        DigitalIn(PTC1, PullUp)};

    static unsigned int sums[3];
    static unsigned int samples;

    if (isInit()) {

        printf("===== Starting test EMG CABLE ====\r\n");

        printf("Connect your EMG cable to the pads and the\r\n");
        printf("3.5mm jack on the external board.\r\n");

        led_blue = led_green = led_red = 1; // Off

        sums[0] = sums[1] = sums[2] = 0;
        samples = 0;
    }

    bool connection_good = true;
    for (size_t i = 0; i < 3; i++) {
        bool signal = !inputs[i].read(); // True by default, false when pulled low
        if (!signal) {
            connection_good = false;
        }

        sums[i] += signal;
    }

    led_red = !(connection_good);
    // Turn led off when one or more connections are broken

    if (samples % 500 == 0) {
        printf("Taken %d samples...\r\n", samples);
    }

    samples++;

    if (samples > 2000) {
        setState(RESULT); // Done testing
    }

    if (isExit()) {

        const float threshold = 0.999f;

        float avg[3];
        connection_good = true;
        for (size_t i = 0; i < 3; i++) {
            avg[i] = (float)(sums[i]) / samples;

            if (avg[i] <= threshold) {
                connection_good = false;
            }
        }

        printf("Connection black: %.3f\r\n", avg[0]);
        printf("Connection red: %.3f\r\n", avg[1]);
        printf("Connection white: %.3f\r\n", avg[2]);

        test_passed = connection_good;

        if (test_passed) {
            printf("TEST PASSED\r\n");
        } else {
            printf("TEST FAILED!\r\n");
            printf("At least one connection is under the threshold of %.3f\r\n", threshold);
        }
    }

}