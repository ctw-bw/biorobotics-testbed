#ifndef CONTROL_H
#define CONTROL_H

#include "state_machine.h"
#include "mbed-scope/scope.h"

#define M_2PI 6.28318530718

/**
 * Main state machine class of this program
 */
class Control : public StateMachine {

public:
    enum State {
        IDLE = 0,
        RESULT,
        EMG,
        EMG_CABLE,
        MOTOR,
        BIOROBOTICS_SHIELD,
    };

    /**
     * Constructor
     */
    Control();

    /**
     * Destructor
     */
    ~Control();

    /**
     * Choose the right state function based on current state
     */
    void run_state() override;

    static float FS; /// Loop rate (Hz)

private:

    void state_idle();
    void state_result();
    void state_emg();
    void state_emg_cable();
    void state_motor();
    void state_biorobotics();

    /**
     * Return alternating true and false, based on state time
     *
     * @param dt Period of blink frequency
     */
    bool blink(int dt) const;

    DigitalIn sw2, sw3; /// Buttons
    DigitalOut led_green, led_red, led_blue; /// Embedded LED
    DigitalOut led_pass, led_fail; /// External LEDs

    BufferedSerial pc;

    bool test_passed; /// True if last test passed

    bool use_scope; /// True if scope should be used

};

#endif // CONTROL_H
