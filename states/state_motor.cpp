#include "control.h"
#include "../motor/motor.h"
#include "../quadrature-encoder/QEI.h"

/**
 * Test motor, motor shield and encoders together.
 */
void Control::state_motor() {

    static Motor motor1(D5, D4);
    static Motor motor2(D6, D7);
    static QEI encoder_a(D10, D11, 8768, QEI::X4_ENCODING),
        encoder_b(D12, D13, 8768, QEI::X4_ENCODING);

    static QEI *encoder1, *encoder2; // Create pointers to encoder
    // instances. We don't know yet which belongs to which.

    static float encoder1_dir, encoder2_dir; // Track if the direction
    // should be flipped

    static float encoder1_prev, encoder2_prev; // Position before spinning

    static bool pass; // Test status

    static unsigned int mode;
    static bool crossed; // True if encoder2 belongs to motor1

    const unsigned int spin_time = 2000;
    const float speed = 0.75f; // PWM intensity
    const float delta = 0.0005f * spin_time; // Minimum
    // displacement in revolutions

    if (isInit()) {

        printf("===== Starting test MOTORS ====\r\n");

        printf("Place your motor shield and biorobotics shield\r\n");
        printf("and connect your motor and external power.\r\n");
        printf("Make sure the encoders are connected to pins\r\n");
        printf("D10 to D13.\r\n");

        motor1.set(0.0);
        motor2.set(0.0);

        encoder_a.reset();
        encoder_b.reset();

        encoder1_dir = encoder2_dir = 1.0f;
        encoder1_prev = encoder2_prev = 0.0f;
        mode = 0;
        pass = true;
        crossed = false;

        led_green = led_blue = 1; // LEDs are pulled up
    }

    // Quickly blink blue LED
    led_blue.write(blink(200));

    if (mode == 0) {
        motor1.set(speed);
        motor2.set(0.0f);

        printf("Setting motor 1 pwm: %.2f\r\n", speed);

        mode++;

    } else if (mode == 1) {

        if (getStateTime() > spin_time) {

            // Check

            // Check which encoder has moved the most
            if (abs(encoder_a.getRevolutions()) > abs(encoder_b.getRevolutions())) {
                encoder1 = &encoder_a;
                encoder2 = &encoder_b;
                printf("Using motor 1: D10 and D11\r\n");
                printf("Using motor 2: D12 and D13\r\n");
            } else {
                encoder1 = &encoder_b; // Flipped
                encoder2 = &encoder_a;
                printf("Using motor 1: D12 and D13\r\n");
                printf("Using motor 2: D10 and D11\r\n");
            }

            if (encoder1->getRevolutions() - encoder1_prev < 0.0f) {
                encoder1_dir = -1.0f; // Output should be flipped
                printf("Flipping direction for motor 1");
            }

            float revs = encoder1_dir * (encoder1->getRevolutions() - encoder1_prev);

            printf("Motor 1 encoder delta (revs): %.2f (Threshold: %.2f)\r\n", revs, delta);

            if (revs < delta) {
              pass = false; // Did not rotate enough
            }

            encoder1_prev = encoder1->getRevolutions();
            encoder2_prev = encoder2->getRevolutions();

            // New mode
            
            motor1.set(-speed);
            motor2.set(0.0f);

            printf("Setting motor 1 pwm: %.2f\r\n", -speed);

            mode++;
        }

    } else if (mode == 2) {

        if (getStateTime() >  2 * spin_time) {

            // Check

            float revs = encoder1_dir * (encoder1->getRevolutions() - encoder1_prev);

            printf("Motor 1 encoder delta (revs): %.2f (Threshold: %.2f)\r\n", revs, -delta);

            if (revs > -delta) {
                pass = false; // Did not rotate enough
            }

            encoder1_prev = encoder1->getRevolutions();
            encoder2_prev = encoder2->getRevolutions();

            // New mode

            motor1.set(0.0f);
            motor2.set(speed);

            printf("Setting motor 2 pwm: %.2f\r\n", speed);

            mode++;
        }

    } else if (mode == 3) {

        if (getStateTime() > 3 * spin_time) {

            // Check

            if (encoder2->getRevolutions() - encoder2_prev < 0.0f) {
                encoder2_dir = -1.0f; // Output should be flipped

                printf("Flipping direction for motor 2");
            }

            float revs = encoder2_dir * (encoder2->getRevolutions() - encoder2_prev);

            printf("Motor 2 encoder delta (revs): %.2f (Threshold: %.2f)\r\n", revs, delta);

            if (revs < delta) {
              pass = false; // Did not rotate enough
            }

            encoder1_prev = encoder1->getRevolutions();
            encoder2_prev = encoder2->getRevolutions();

            // New mode

            motor1.set(0.0f);
            motor2.set(-speed);

            printf("Setting motor 2 pwm: %.2f\r\n", -speed);

            mode++;
        }

    } else if (mode == 4) {

        if (getStateTime() > 4 * spin_time) {

            // Check

            float revs = encoder2_dir * (encoder2->getRevolutions() - encoder2_prev);

            printf("Motor 2 encoder delta (revs): %.2f (Threshold: %.2f)\r\n", revs, -delta);

            if (revs > -delta) {
              pass = false; // Did not rotate enough
            }

            // New mode

            mode++;

            setState(RESULT); // Test is over
        }
        
    }

    if (isExit()) {

        test_passed = pass;

        if (test_passed) {
            printf("TEST PASSED\r\n");
        } else {
            printf("TEST FAILED!\r\n");
            printf("Encoder deltas not all reached\r\n");
        }

        motor1.set(0.0f);
        motor2.set(0.0f);
    }

}