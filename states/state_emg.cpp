#include "control.h"
#include "../biquad-filter/biquad_filter.h"

/**
 * Test EMG shield
 */
void Control::state_emg() {

    // Test wave
    const float freq = 12.0f;

    static AnalogIn a0(A0);
    static PwmOut pwm(D9);
    static BiquadFilter filter_hp(FS, 0.85f * freq, BiquadFilter::TYPE_HIGH_PASS);
    static BiquadFilter filter_lp(FS, 1.15f * freq, BiquadFilter::TYPE_LOW_PASS);
    static BiquadFilter filter_lp_slow(FS, 5.0f, BiquadFilter::TYPE_LOW_PASS);

    // Count total samples and bad samples
    static unsigned int samples;
    static float error_sum_sq;

    // Pointer to scope
    static Scope* scope = nullptr;

    if (isInit()) {

        printf("===== Starting test EMG SHIELD ====\r\n");

        printf("Place your EMG shield and connect the calibration\r\n");
        printf("cable from the CAL pins to the 3.5mm jack.\r\n");
        printf("Place a jumper on the `D9` configuration.\r\n");
        printf("And place a jumper for the A0 output.\r\n");

        if (use_scope && !scope) {

            printf("\r\nStarting scope, waiting for USB connection on 'K64F' plug...\r\n");


            // Allocate new Scope instance
            // This will run the constructor and block until a connection was established

            scope = new Scope(4);
            // Don't bother with deallocation because a microcontroller only stops when powered off
        }

        if (scope) {
            printf("Scope legend: 1. Output 2. EMG 3. Filt. EMG 4. Envelope error\r\n");
        }

        pwm.period(1.0f / freq);
        pwm.write(0.0f);

        filter_hp.reset(0.0f);
        filter_lp.reset(0.0f);
        filter_lp_slow.reset(0.0f);

        samples = 0;
        error_sum_sq = 0.0f;

        led_green = led_red = 1; // LEDs are pulled up
    }

    // Quickly blink blue LED
    led_blue.write(blink(200));

    // Generate wave
    size_t time_blocks = getStateTime() / 1000; // Count seconds

    // Alternate between writing a wave and writing zero
    float signal_envelope = time_blocks % 2 == 0 ? 1.0f : 0.0f;

    // PWM duty cycle is reset upon writing, so only update
    // when needed instead of updating continously.
    float pwm_value = signal_envelope * 0.5f; // 50% PWM is a symmetric block wave
    static float pwm_value_old = pwm_value;
    if (pwm_value_old != pwm_value) {
        pwm.write(pwm_value);
        pwm_value_old = pwm_value;
    }

    float emg = a0.read();

    float emg_filt = filter_lp.sample(emg);
    emg_filt = filter_hp.sample(emg_filt);
    emg_filt = 2.0f * abs(emg_filt); // Rectify
    emg_filt = filter_lp_slow.sample(emg_filt);
    emg_filt *= 11.0f; // Amplify to about 1

    // Verify signal
    // We cannot compare the produced and measured signals directly
    // because of delays due to the analog filters inside the EMG shield
    // We verify the signal by leaving only the frequency of the output
    // signal. This is then rectified 
    float error = signal_envelope - emg_filt;

    samples++;
    error_sum_sq += error * error;

    if (samples % 1000 == 0) {
        printf("Taken %d samples...\r\n", samples);
    }

    // If scope was allocated
    if (scope) {
        size_t i = 0;
        scope->set(i++, signal_envelope);
        scope->set(i++, emg);
        scope->set(i++, emg_filt);
        scope->set(i++, error);
        scope->send();
    }

    // Exit
    if (samples >= 8000) {
        setState(RESULT); // Test is over
    }

    if (isExit()) {

        float error_sq = error_sum_sq / (float)samples;

        const float error_limit = 0.25f;

        printf("Samples: %d\r\nSum squared error: %.3f (Threshold: %.3f)\r\n",
            samples, error_sq, error_limit);

        test_passed = (error_sq <= error_limit);

        if (test_passed) { // Too much error
            printf("TEST PASSED\r\n");
        } else {
            printf("TEST FAILED!\r\n");
            printf("Error between output and measured signals too big\r\n");
        }
        
        pwm.write(0.0f);
    }
}