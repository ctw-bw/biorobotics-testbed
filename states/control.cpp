#include "mbed.h"
#include <cstdint>

#include "control.h"

float Control::FS = 750.0f;

// Constructor
Control::Control() :
    StateMachine(0),
    sw2(SW2), sw3(SW3),
    led_green(LED_GREEN), led_red(LED_RED), led_blue(LED_BLUE),
    led_pass(PTE26), led_fail(PTC5),
    // Note: PTE26 = LED_GREEN
    pc(USBTX, USBRX) {

    pc.set_baud(115200);
    pc.set_blocking(false);

    use_scope = false;
    test_passed = false;
}

// Destructor
Control::~Control() {
    
    // Nothing, really
}

// State selection
void Control::run_state() {
    switch (getState()) {
        case IDLE:
            state_idle();
            return;
        case RESULT:
            state_result();
            return;
        case EMG:
            state_emg();
            return;
        case EMG_CABLE:
            state_emg_cable();
            return;
        case MOTOR:
            state_motor();
            return;
        case BIOROBOTICS_SHIELD:
            state_biorobotics();
            return;
    }
}

bool Control::blink(int dt) const {
    return getStateTime() % (2*dt) > dt;
}

// ------------- IDLE -------------

/**
 * Default state, use to go to different states
 */
void Control::state_idle() {

    static DigitalIn buttons[4] = {DigitalIn(PTC0, PullUp), DigitalIn(PTC7, PullUp), DigitalIn(PTC8, PullUp), DigitalIn(PTC9, PullUp)};
    // Button order was changed to match with connections after assembly

    static size_t buttons_debounce[4];

    if (isInit()) {
        printf("\r\n\r\nRunning test bed\r\n\r\n");

        printf("Use serial input to select test\r\n");
        printf("1 - EMG SHIELD\r\n");
        printf("2 - EMG CABLE\r\n");
        printf("3 - MOTORS\r\n");
        printf("4 - BIOROBOTICS SHIELD\r\n");

        printf("\r\n");

        printf("s - Enable HID scope output during EMG shield test\r\n");

        printf("\r\n");

        for (size_t i = 0; i < 4; i++) {
            buttons_debounce[i] = 0;
        }

        led_blue = led_red = 1; // LEDs are pulled up
    }

    // Slowly blink green LED
    led_green.write(blink(1000));

    // Handle mode buttons
    for (size_t i = 0; i < 4; i++) {
        
        if (!buttons[i].read()) { // If pressed
            buttons_debounce[i]++;
        } else {
            buttons_debounce[i] = 0;
        }

        if (buttons_debounce[i] > 100) {
            switch (i) {
            case 0:
                setState(EMG);
                break;
            case 1:
                setState(EMG_CABLE);
                break;
            case 2:
                setState(MOTOR);
                break;
            case 3:
                setState(BIOROBOTICS_SHIELD);
                break;
            }
        }
    }

    // Handle serial input
    char c;
    size_t bytes_read = pc.read(&c, 1);

    if (bytes_read > 0) {

        switch (c) {
        case '1':
            setState(EMG);
            break;
        case '2':
            setState(EMG_CABLE);
            break;
        case '3':
            setState(MOTOR);
            break;
        case '4':
            setState(BIOROBOTICS_SHIELD);
            break;
        case 's':
            use_scope = true;
            printf("Enabled scope\r\n");
            break;
        }
    }
}

/**
 * Display the result of a test
 */
void Control::state_result() {

    static DigitalOut* led = nullptr;

    if (isInit()) {
        // Choose led
        led = test_passed ? &led_pass : &led_fail;

        led_red = led_green = led_blue = 1; // Turn all off

        led_pass.write(0);
        led_fail.write(0);
    }

    led->write(blink(50)); // Blink LED rapidly

    if (getStateTime() > 3000) {
        setState(IDLE);
    }

    if (isExit()) {
        led->write(0); // External LEDs are pulled down
    }
}
