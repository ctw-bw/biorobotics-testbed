#include "control.h"

/**
 * Test BioRobotics shield
 */
void Control::state_biorobotics() {

    static AnalogIn pots[2] = {AnalogIn(A4), AnalogIn(A5)};
    static DigitalIn buttons[2] = {DigitalIn(D2, PullUp), DigitalIn(D3, PullUp)};

    static float pot_prev[2];
    const float pot_min = 0.05, pot_max = 0.95; // Min and max that should be reached
    static bool pot_mined[2], pot_maxed[2];
    static bool pot_bigskip[2];
    static size_t pot_spam[2];

    const float skip_threshold = 0.1f;

    static size_t button_counter[2];
    static bool but_pressed[2];

    if (isInit()) {

        printf("===== Starting test BIOROBOTICS SHIELD ====\r\n");

        printf("Connect BioRobotics shield and connect the potmeters to\r\n");
        printf("A4 and A5, and the buttons to D2 and D3.\r\n");
        printf("Test the LEDs manually by connecting them directly to \r\n");
        printf("+3.3V or +5V.\r\n");

        for (size_t i = 0; i < 2; i++) {

            pot_mined[i] = pot_maxed[i] = false;
            pot_bigskip[i] = false;
            pot_prev[i] = pots[i].read();
            pot_spam[i] = 0;

            but_pressed[i] = false;
            button_counter[i] = 0;
        }
    }

    // Quickly blink blue LED
    led_blue.write(blink(200));

    bool passed = true;

    float pot_val[2];

    for (size_t i = 0; i < 2; i++) {

        pot_val[i] = pots[i].read();

        // Update min / max
        if (pot_val[i] < pot_min) {
            if (!pot_mined[i]) {
                printf("Pot %d reached past required minimum of %.2f\r\n", i, pot_min);
            }
            pot_mined[i] = true;
        }
        if (pot_val[i] > pot_max) {
            if (!pot_maxed[i]) {
                printf("Pot %d reached past required maximum of %.2f\r\n", i, pot_max);
            }
            pot_maxed[i] = true;
        }

        // Look for skips

        float pot_step = pot_val[i] - pot_prev[i];

        if (abs(pot_step) > skip_threshold) {
          pot_bigskip[i] = true;

            if (pot_spam[i]++ < 10) {
                printf("Discontinuity found, pot %d signal stepped from %.2f to %.2f\r\n",
                    i, pot_prev[i], pot_val[i]);
            }
        }

        pot_prev[i] = pot_val[i];

        // Test buttons
        if (!buttons[i].read()) {
            button_counter[i]++;
        } else {
            button_counter[i] = 0;
        }

        if (button_counter[i] > 50) {

            if (!but_pressed[i]) {
                printf("Button %d pressed\r\n", i);
            }
            but_pressed[i] = true; // Button was pressed properly
        }

        // Check
        if (!pot_mined[i] || !pot_maxed[i] || pot_bigskip[i] || !but_pressed[i]) {
            passed = false;
        }
    }

    if (passed) {
        setState(RESULT); // Stop testing, we've seen it all
    }

    if (getStateTime() > 30000) {
        setState(RESULT);

        printf("Timeout, stopping test\r\n");
    }

    // Every second
    static uint32_t last_update = 0;
    if (getStateTime() - last_update >= 1000) {
        printf("         Pot 1: %.2f, Pot 2: %.2f, Button 1: %d, Button 2: %d\r\n",
            pot_val[0], pot_val[1], !buttons[0].read(), !buttons[1].read());

        last_update = getStateTime();
    }

    if (isExit()) {

        test_passed = passed;

        // Check
        if (test_passed) {
            printf("TEST PASSED\r\n");
        } else {

            printf("TEST FAILED!\r\n");

            for (size_t i = 0; i < 2; i++) {

                if (!pot_mined[i]) {
                    printf("Pot %d did not reach minimum of %.2f\r\n", i, pot_min);
                }
                if (!pot_maxed[i]) {
                    printf("Pot %d did not reach maximum of %.2f\r\n", i, pot_max);
                }

                if (pot_bigskip[i]) {
                    printf("Pot %d hit a discontinuity\r\n", i);
                }

                if (!but_pressed[i]) {
                    printf("Button %d was not pressed\r\n", i);
                }
            }
        }

        printf("\r\n\r\n");

    }
}