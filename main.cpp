#include "mbed.h"
#include "states/control.h"

using namespace std::chrono;

Timer t;

int main()
{
    t.start();

    Control machine;

    duration<float> loop_time(1.0f / machine.FS);

    while (true) {

        auto next = t.elapsed_time() + loop_time;

        machine.run();

        // Busy wait until the looptime (including execution) has passed
        while (t.elapsed_time() < next) {}
    }
}
