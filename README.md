# BioRobotics Test Bed

This project contains an application that, in combination with special hardware, can test the parts used in the BioRobotics minor.

![test bed photo](testbed.jpg)

## Instructions

 1. Connect your to-be tested hardware. Mind any test-specific instructions below.
 2. Select the appropriate test, either via the push buttons or via the serial connection.
    * The serial connection has baudrate 115200. Use the 'SDA' plug for the serial terminal.
 3. Wait or perform the manual part of the test.
 4. View the results. The green LED will blink quickly to indicate a pass, while the red LED will blink when the test failed. You can also view the result from the terminal, including details in case the test failed.

| Blue LED (on-board) | Green LED        | Red LED          | State                         |
|---------------------|------------------|------------------|-------------------------------|
|                     | Blinking slowly  |                  | Waiting on test selection     |
| Blinking quickly    | On               |                  | Executing test                |
|                     | Blinking quickly |                  | Last test passed successfully |
|                     |                  | Blinking quickly | Last test failed              |

*Note:* you can safely attach the hardware for multiple tests at once. For example, you could place your entire shield stack.

## Tests

### EMG Shield

 1. Attach your EMG shield. Only a single shield will be tested, on A0.
 2. Connect the calibration cable.
 3. Using jumpers, select: "A0", "3.3V" and "D9" (next to the analog pin selection).
 4. Run the test.
    * Type `s` before starting the EMG test to enable output over HID-USB to [uScope](https://bitbucket.org/ctw-bw/uscope). *Note*: when this enabled, the program will block until a USB connection was found.

**Warning:** should you have multiple EMG shields attached, make sure sure you have only *one* shield set to A0. Do not select other analog pins!
Simply detach the analog selector jumper for additional EMG shields entirely.

The EMG shield has a built-in calibration feature, which we can use test the amplification. A special cable is used to connect the CAL pins to the 3.5mm jack.
By putting a jumper on the D9 configuration, a PWM signal can be fed through the CAL-circuit, which is a voltage divider. This brings the square PWM wave
down to max ~50 mV. The PWM is set to 12 Hz.

An intermitted wave is produced and compared to the measured input. An envelope of the measured signal is made and a squared sum error is computed. The test
passes when this squared error is within a threshold.

### EMG Cable

 1. Attach your cable to the copper pads and to the external female 3.5mm jack (*not* to the shield if it's attached). No EMG shield needs to be attached.
 2. Run the test.
     * The LED will light up red while it measures a connection on all three wires. If you move the cable and the LED turns off (if only briefly), this indicates a problem with the cable.

The EMG cable is attached to both an external 3.5mm socket and three copper pads. The pads are grounded and the jack is attached to 3 digital inputs.
The inputs are set in the pull-up mode, so if the cable is in good condition the measured inputs are pulled low. The average of the signal is taken 
over some time and compared to a threshold.

Note: this would not find a short in the cable (if e.g. two wires make contact with each other).

### Motors

 1. Attach your motor shield and the BioRobotics shield.
 2. Connect the motor and power as you would normally.
 3. Connect the encoders on the BioRobotics shield to D10 and D11, and D12 and D13. You can use the provided wire. It doesn't matter which encoder connects to which.
 4. Run the test.
     * Verify both motors turn once in each direction. The program cannot distinguish between a broken encoder and a broken motor.

The motors are each going to turn clockwise and counter-clockwise for a bit. The MCU will read from the encoders and make sure they have changed sufficiently.  
The MCU will figure out automatically which encoder belongs to which motor. Likewise, if the encoder reads in the opposite direction this is accounted for too.

### BioRobotics Shield

 1. Attach your BioRobotics shield.
 2. Connect the potmeters to A4 and A5 and the buttons to D2 and D3 (the order does not matter for either).
 3. Start the test.
 4. As the test is running, do the following (in no particular order):
     * Firmly press BUT1
     * Firmly press BUT2
     * Turn POT1 to the minimum and maximum
     * Turn POT2 to the minimum and maximum

The test will finish when the above has been performed, or until 30 seconds have elapsed.

The LEDs have to be tested manually. This is easy: just connect the LEDs to the +3.3V or +5V pin. (The shield has internal pull resistors.)

During the test it's validated that the min and max values of the potmeters are sufficiently far apart and there are no jumps, and the buttons can be pressed continuously for more than a few milliseconds.

## Diagram

![circuit](circuit.png "Testbed Circuit")

View on [https://circuit-diagram.org](https://crcit.net/c/f2a27957f4fa48f38a8222b729e41fe9).

## Build

Build the source using MBED Studio or the MBED online compiler.

## Modules

MBED Studio has its own way of including modules. Look at the .lib files in the root for links.
